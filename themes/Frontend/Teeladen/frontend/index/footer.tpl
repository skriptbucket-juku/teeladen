{extends file="parent:frontend/index/footer.tpl"}

{block name="frontend_index_shopware_footer_logo"}{/block}

{block name='frontend_index_footer_copyright'}
    <p style="padding: 1.25rem 0px; text-align:center; font-size:80%;">
	* Alle Preise inkl. gesetzl. Mehrwertsteuer zzgl. Versandkosten und ggf. Nachnahmegebühren, wenn nicht anders beschrieben
	</p>
{/block}
