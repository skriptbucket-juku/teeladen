{extends file='parent:frontend/index/header.tpl'}

{* externes Stylesheet *}
{block name="frontend_index_header_css_screen" append}
<link type="text/css" media="all" rel="stylesheet"
   href="{link file='frontend/_public/src/css/skriptstube.css'}" />
{/block}


{* externes Javascript *}
{block name="frontend_index_header_javascript_jquery" append}
   <script type="text/javascript" src="{link file='frontend/_public/src/css/skriptstube.js'}"></script>
{/block}
