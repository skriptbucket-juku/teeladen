{namespace name="frontend/index/menu_footer"}
<div id="markenlogos"></div>
{* Service hotline *}
{block name="frontend_index_footer_column_service_hotline"}
    <div class="footer--column column--hotline is--first block">
        {block name="frontend_index_footer_column_service_hotline_headline"}
            <div class="column--headline">{s name="sFooterServiceHotlineHead"}{/s}</div>
        {/block}

        {block name="frontend_index_footer_column_service_hotline_content"}
            <div class="column--content">
                <p class="column--desc">{s name="sFooterServiceHotline"}{/s}</p>

				<p>Oder schreiben Sie uns eine E-Mail: <a href="mailto:info@teeladen-weiden.de">info@teeladen-weiden.de</a></p>

            </div>
        {/block}
    </div>
{/block}

{block name="frontend_index_footer_column_service_menu"}
    <div class="footer--column column--menu block">
        {block name="frontend_index_footer_column_service_menu_headline"}
            <div class="column--headline">{s name="sFooterShopNavi1"}{/s}</div>
        {/block}

        {block name="frontend_index_footer_column_service_menu_content"}
            <nav class="column--navigation column--content">
                <ul class="navigation--list" role="menu">
                    {block name="frontend_index_footer_column_service_menu_before"}{/block}
                    {foreach $sMenu.bottom as $item}

                        {block name="frontend_index_footer_column_service_menu_entry"}
                            <li class="navigation--entry" role="menuitem">
                                <a class="navigation--link" href="{if $item.link}{$item.link}{else}{url controller='custom' sCustom=$item.id title=$item.description}{/if}" title="{$item.description|escape}"{if $item.target} target="{$item.target}"{/if}>
                                    {$item.description}
                                </a>

                                {* Sub categories *}
                                {if $item.childrenCount > 0}
                                    <ul class="navigation--list is--level1" role="menu">
                                        {foreach $item.subPages as $subItem}
                                            <li class="navigation--entry" role="menuitem">
                                                <a class="navigation--link" href="{if $subItem.link}{$subItem.link}{else}{url controller='custom' sCustom=$subItem.id title=$subItem.description}{/if}" title="{$subItem.description|escape}"{if $subItem.target} target="{$subItem.target}"{/if}>
                                                    {$subItem.description}
                                                </a>
                                            </li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </li>
                        {/block}
                    {/foreach}

                    {block name="frontend_index_footer_column_service_menu_after"}{/block}
                </ul>
            </nav>

        {/block}
    </div>
{/block}

{block name="frontend_index_footer_column_information_menu"}
    <div class="footer--column column--menu block">
        {block name="frontend_index_footer_column_information_menu_headline"}
            <div class="column--headline">Unsere Zahlungsarten</div>

			<table>
				<tr>
					<td style="padding:0px;">
						<img src="{link file = 'frontend/_public/src/img/vorkasse.png' fullPath}"/>
					</td>
					<td style="padding:0px;">
						<img src="{link file = 'frontend/_public/src/img/rechnung.png' fullPath}"/>
					</td>
                    <td style="padding:0px;">
                        <img src="{link file = 'frontend/_public/src/img/paypal.png' fullPath}"/>
                    </td>
				</tr>
			</table>

            <div class="column--headline">Vorteile</div>
            <p>
                <i class="icon--star"></i> Große Auswahl
                </br>
                <i class="icon--star"></i> Ausgewählte Teesorten
                </br>
                <i class="icon--star"></i> Schneller Versand mit Frischegarantie
                </br>
                <i class="icon--star"></i> Top Service und Beratung
            </p>
        {/block}

        {block name="frontend_index_footer_column_information_menu_content"}

        {/block}
    </div>
{/block}

{block name="frontend_index_footer_column_newsletter"}
    <div class="footer--column column--newsletter is--last block">
        {block name="frontend_index_footer_column_newsletter_headline"}
        {/block}

        {block name="frontend_index_footer_column_newsletter_content"}
            <div class="column--content">
                <p class="column--desc">

                    <img style="max-height:80px; margin:50px auto 0px auto;" src="{link file = 'frontend/_public/src/img/logo.png' fullPath}"/>

                </p>
            </div>
        {/block}
    </div>
{/block}
